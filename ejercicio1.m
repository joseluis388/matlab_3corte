

T=4;
ts=1e-2;
t=-3*T:ts:3*T;
omega=pi/2;
x_fs=zeros(1, length(t));

N_zero=0;
N_one=3;
N_two=10;
N_three=80;
N_four=1000;

x_fs_zero=zeros(1, length(t));
x_fs_one=zeros(1, length(t));
x_fs_two=zeros(1, length(t));
x_fs_three=zeros(1, length(t));
c_0=0;

for k=-1000:N_four
 c_k=((2*1i)/(pi*k))*(1^k);
 omega_k=omega*k;
 
 x_fs=x_fs+exp(1i*omega_k*t).*c_k;
 
  if k==0
     c_k=c_0;
  end
  
 if k== N_one
     x_fs_one=x_fs;
 end
 if k== N_two
     x_fs_two=x_fs;
 end
 
 if k== N_three
     x_fs_three=x_fs;
 end
     
 
end

% Graphs
figure()
subplot(2,2,1)
plot(t,x_fs_one);
grid on
xlim([-3*T, 3*T])
ylim([-2.5, 2.5])
xlabel('t, s')
ylabel('x(t)')
title('N_max=3')

subplot(2,2,2)
plot(t,x_fs_two);
grid on
xlim([-3*T, 3*T])
ylim([-2.5, 2.5])
xlabel('t, s')
ylabel('x(t)')
title('N_max=10')

subplot(2,2,3)
plot(t,x_fs_three);
grid on
xlim([-3*T, 3*T])
ylim([-2.5, 2.5])
xlabel('t, s')
ylabel('x(t)')
title('N_max=80')

subplot(2,2,4)
plot(t,x_fs);
grid on
xlim([-3*T, 3*T])
ylim([-2.5, 2.5])
xlabel('t, s')
ylabel('x(t)')
title('N_max=1000')

title('N_max=1000')
